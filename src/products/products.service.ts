import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

// eslint-disable-next-line prefer-const
let products: Product[] = [
  {
    id: 1,
    name: 'Mocca',
    price: 60,
  },
  {
    id: 2,
    name: 'Latea',
    price: 60,
  },
  {
    id: 3,
    name: 'Espesso',
    price: 60,
  },
];
let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto, //login,name,password
    };

    products.push(newProduct);
    return products;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    const updateUser: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      {
        id: 1,
        name: 'Mocca',
        price: 60,
      },
      {
        id: 2,
        name: 'Latea',
        price: 60,
      },
      {
        id: 3,
        name: 'Espesso',
        price: 60,
      },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
